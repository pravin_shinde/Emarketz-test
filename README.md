# Read Me

Clone Project using git clone https://gitlab.com/pravin_shinde/Emarketz-test.git

### Installation

-   Install the dependencies and devDependencies and start the server.

```sh
$ cd Emarketz-test
$ composer update
```

-   Create .env file and Set up database in .env file.
-   run migration command php artisan

```sh
$ php artisan migrate
```

# New Features!

-   New Product Section
-   Product List
-   Cart
-   Coupon Code Section

Developed Using:

-   Php Larael And Mysql
-   Frontend With Bootstrap
-   Other Jquery ,Parsley for validation , toastr for Toast Notification

### Plugins

Emarketz is currently extended with the following plugins. Instructions on how to use them in your own application are linked below.

| Plugin    | Url                                 |
| --------- | ----------------------------------- |
| Php 7     | https://www.php.net/                |
| Mysql     | https://www.mysql.com/              |
| Laravel 7 | https://laravel.com/                |
| Jquery    | https://code.jquery.com/            |
| Parsleyjs | https://parsleyjs.org/              |
| Toastr    | https://github.com/CodeSeven/toastr |

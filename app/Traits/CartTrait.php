<?php

namespace App\Traits;

use App\Cart;

trait CartTrait
{
    public function get_cart_info()
    {
        $cart = Cart::with('product_details:id,product_name')->where('user_id', $this->get_user_id())->get()->toArray();
        $sum = 0;
        foreach ($cart as $item) {
            $sum += $item['total_amount'];
        }
        $cartData['items'] = $cart;
        $cartData['total_amount'] = $sum;
        return $cartData;
    }
}

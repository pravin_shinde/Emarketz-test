<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart_details';
    protected $fillable = ['user_id', 'item_id', 'item_type', 'item_price', 'qty', 'total_amount'];


    public function product_details()
    {
        return $this->hasOne('App\Product', 'id');
    }
}

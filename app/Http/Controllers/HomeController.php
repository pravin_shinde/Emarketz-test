<?php

namespace App\Http\Controllers;

use App\Product;
use App\Traits\CartTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class HomeController extends Controller
{
    use CartTrait;

    public function index()
    {
        $cartData = $this->get_cart_info();
        // dd($cartData);
        $products = Product::all();
        return view('index', compact('products', 'cartData'));
    }

    public function addNewProduct(Request $request)
    {
        $rules = [
            'price' => 'integer',
            'title' => [
                'required',
                Rule::unique('products', 'product_name')
            ],
        ];

        $validator =  Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $insertData = ['product_name' => $request->title, 'price' => $request->price];
        Product::create($insertData);
        return back()->with('status', 'Product Created');
    }

    public function addToCart(Request $request)
    {
    }
}

<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use App\Traits\CartTrait;
use Illuminate\Http\Request;

class CartController extends Controller
{
    use CartTrait;

    public function addToCart(Request $request)
    {
        // dd($this->get_cart_info());

        //check if item exists in cart for the user

        $cart = Cart::where(['user_id' => $this->get_user_id(), 'item_id' => $request->item_id, 'item_type' => $request->item_type])->first();
        // dd($this->get_cart_info());

        if (!$cart) {

            $product = Product::find($request->item_id)->first();
            if (!$product) {
                return response()->json([
                    'error' => 1,
                    'error_message' => 'Product Does Not Exists',
                ]);
            }

            Cart::create([
                'user_id' => $this->get_user_id(),
                'item_id' => $request->item_id,
                'item_type' => $request->item_type,
                'item_price' => $product->price,
                'qty' => $request->qty,
                'total_amount' =>  $product->price * $request->qty,
            ]);

            return response()->json([
                'error' => 0,
                'error_message' => '',
                'cart_info' => $this->get_cart_info()
            ]);
        }

        $product = Product::find($request->item_id)->first();

        $cart->qty = $cart->qty + $request->qty;
        $cart->item_price =  $product->price;
        $cart->total_amount = $product->price * ($cart->qty + $request->qty);

        $cart->save();
        return response()->json([
            'error' => 0,
            'error_message' => '',
            'cart_info' => $this->get_cart_info()
        ]);
        //Update Cart
    }

    public function applyCoupon(Request $request)
    {
        $cart = $this->get_cart_info();

        if ($request->coupon_type == 'percentage') {

            $cartDiscountAmount = $cart['total_amount'] * ($request->coupon_value / 100);
        } else {
            //flat
            $cartDiscountAmount = $request->coupon_value;
        }
        $totalAmountAfterDiscount = $cart['total_amount'] - $cartDiscountAmount;
        $cart['discount_amount'] = round($cartDiscountAmount);
        $cart['amount_after_discount'] = round($totalAmountAfterDiscount);

        return response()->json([
            'error' => 0,
            'error_message' => '',
            'cart_info' => $cart
        ]);
    }

    // public function get_cart_info()
    // {
    //     $cart = Cart::with('product_details:id,product_name')->where('user_id', $this->get_user_id())->get()->toArray();
    //     $sum = 0;
    //     foreach ($cart as $item) {
    //         $sum += $item['total_amount'];
    //     }
    //     $cartData['items'] = $cart;
    //     $cartData['total_amount'] = $sum;
    //     return $cartData;
    // }
}

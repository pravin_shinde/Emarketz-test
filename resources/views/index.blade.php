<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Emarketz</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
            <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
        <script>
            var base_url = '{{url('')}}';
        </script>
    </head>
    <body>

        <header>
            <div class="navbar navbar-dark bg-dark box-shadow">
              <div class="container d-flex justify-content-between">
                <a href="#" class="navbar-brand d-flex align-items-center">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg>
                  <strong>Emarketz</strong>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              </div>
            </div>
          </header>

          <main role="main">
            <section class="jumbotron text-center">
                <div class="container">
                    <div class="card text-center pr-5 pl-5">
                        <div class="card-header">
                          Cart Info
                        </div>
                        <div class="card-body">
                          <h5 class="card-title">Cart Details</h5>
                          <table class="table table-hover table-bordered cart-table">
                            <thead>
                              <tr>
                                <th scope="col">Item Name</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Per Unit Price</th>
                                <th scope="col">Total Price</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($cartData['items'] as $item)
                                    <tr>
                                        <th scope="row">{{$item['product_details']['product_name'] }}</th>
                                        <td>{{ $item['qty']}}</td>
                                        <td>{{ $item['item_price']}}</td>
                                        <td>{{ $item['total_amount']}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                               <tr>
                                    <th scope="row">Total Amount</th>
                                    <td colspan="2"></td>
                                    <td><label class="price-to-pay" id="totalAmount">{{$cartData['total_amount']}}</label></td>
                                </tr>
                                <tr>
                                    <th scope="row">Coupon Code - ABC</th>
                                    <td colspan="2">
                                        <select name="" id="selCouponType">
                                            <option value="">Select Coupon Type</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="flat">Flat</option>
                                        </select>
                                        <input type="number" min="1" placeholder="Discount Value" id="dicountValue">
                                        <input type="button" id="applyCoupon" value="apply">
                                    </td>
                                    <td><label for="" id="lblDiscountValue">0</label></td>
                                </tr>
                                <tr>
                                    <th scope="row">Final Price</th>
                                    <td colspan="2"></td>
                                    <td><label class="price-to-pay" id="finalPrice">{{ $cartData['total_amount'] }}</label></td>
                                </tr>
                                <tr>
                                    <td colspan="4"><input type="button" class="btn-primary" value="Checkout"></td>
                                </tr>
                            </tfoot>
                          </table>
                          
                        </div>
                    </div>
                    <div class="card text-center pr-5 pl-5 ">
                        <div class="card-header">
                          Products
                        </div>
                        <div class="card-body">
                          <h5 class="card-title">New Product Item</h5>

                          @if(count($errors))
                        
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li> {{$error}} </li>
                                  @endforeach
                              </ul>
                          </div>
                            
                        @endif
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                          
                        <form class="form-inline" action="{{ route('add_new_product')}}" method="POST" id="frmNewProduct" data-parsley-validate="">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="inpTitle">Title</label>
                                        <input id="inpTitle" name="title" type="text" class="form-control" requiredd value="" >
                                        <div class="invalid-feedback">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="inpPricePerUnit">Per Unit Price</label>
                                        <input id="inpPricePerUnit" name="price" type="number" min="1" class="form-control" data-parsley-type="number" requiredd value="" >
                                        <div class="invalid-feedback"> </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <button type="submit" id="btnNewProduct" class="btn btn-primary mt-4 pull-right">Add</button>
                                    </div>
                                </div>
                            </div>
                          </form>
                        </div>
                    </div>

                    <div class="album py-5 bg-light">
                        <div class="container">
                          <div class="row">
                              @foreach ($products as $product)
                                <div class="col-md-3">
                                    <div class="card mb-4 box-shadow">
                                        <img class="card-img-top" src="https://pesmcopt.com/admin-media/images/dummy-product-image.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <div class="d-flex justify-content-between text-center">
                                                <div class="row">
                                                    <label>{{$product->product_name}}</label>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" onclick="addToCart({{$product->id}})" class="btn btn-sm btn-primary">Add To Cart</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              @endforeach
                          </div>
                        </div>
                      </div>
                </div>
             
              </section>
          </main>
          
        <!-- JS, Popper.js, and jQuery -->
        <script  src="https://code.jquery.com/jquery-3.5.1.min.js"  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js" integrity="sha512-eyHL1atYNycXNXZMDndxrDhNAegH2BDWt1TmkXJPoGf1WLlNYt08CSjkqF5lnCRmdm3IrkHid8s2jOUY4NIZVQ==" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
      
        <script src="{{asset('assets/js/index.js')}}"></script>
    </body>
</html>
$(document).ready(function () {
    $('#frmNewProduct').parsley();


    // $("#frmNewProduct").on('submit', function (e) {

    //     var form = $(this).closest('form');
    //     var instance = form.parsley();
    //     var formData = form.serialize();
    //     console.log(instance.isValid());
    //     if (!instance.isValid()) {

    //     }
    // });

    $('#applyCoupon').on('click', function (e) {

        var couponType = $('#selCouponType').val();
        var couponValue = $('#dicountValue').val();

        if (couponType == '' || couponValue == '')
            return false;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Accept': "application/json",
            }
        });

        var url = base_url + '/apply-coupon';

        var postData = {
            'coupon_type': couponType,
            'coupon_value': couponValue
        };

        $.ajax({
            type: "POST",
            url: url,
            data: postData,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            success: function (response) {
                // console.log(response)
                if (response.error) {
                    toastr.error(response.error_message, 'Error')
                } else {
                    $('#totalAmount').html(response.cart_info.total_amount);
                    $('#lblDiscountValue').html(response.cart_info.discount_amount);
                    $('#finalPrice').html(response.cart_info.amount_after_discount);
                    toastr.success('Coupon Applied', 'Success');
                }
            }
        });
    });

});

function addToCart(id) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Accept': "application/json",
        }
    });
    var url = base_url + '/add-to-cart';

    var postData = {
        'item_id': id,
        'item_type': 'product',
        'qty': 1
    };

    $.ajax({
        type: "POST",
        url: url,
        data: postData,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        success: function (response) {
            // console.log(response)
            if (response.error) {
                toastr.error(response.error_message, 'Error')
            } else {
                toastr.success('Added To Cart', 'Success');
                bindCartItems(response.cart_info.items);
            }
        }
    });
}

function bindCartItems(items) {
    // console.log(items);
    $('.cart-table tbody').empty();

    var trs = '';
    $.each(items, function (key, data) {

        trs += ' <tr>\
                                <th scope="row">' + data.product_details['product_name'] + '</th>\
                                <td>' + data.qty + '</td>\
                                <td>' + data.item_price + '</td>\
                                <td>' + data.total_amount + '</td>\
                            </tr> ';
    })
    $('.cart-table tbody').append(trs);
}
